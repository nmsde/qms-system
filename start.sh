#!bin/bash


# install forever
npm install forever -g

cd /etc/init.d/
nano qmdService
chmode 755 qmsService
### Service
# install forever and configure the service

#!/bin/sh
#/etc/init.d/qmsService
export PATH=$PATH:/usr/local/bin
export NODE_PATH=$NODE_PATH:/usr/local/lib/node_modules

case "$1" in
start)
exec PORT=8000 forever --sourceDir=/home/pi/Documents/qms-system -p /home/pi/Documents/qms-system service.js  #scriptarguments
;;
stop)
exec forever stop --sourceDir=/home/pi/Documents/qms-system server.js
;;
*)
echo "Usage: /etc/init.d/qmsService {start|stop}"
exit 1
;;
esac
exit 0

# after creating server we need to access /etc/rc.local and add command to run service
sudo nano /etc/rc.local

## add line before exit 0
/etc/init.d/qmsService start
